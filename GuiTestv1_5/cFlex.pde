import cc.arduino.*;

class cFlex
{
  int flexMin = 1000;
  int flexMax = 0;
  int analog;
  boolean calibrated = false;
  
  cFlex(int entryPin)
  {
    analog = entryPin;
  }
  
  float getValueRaw()
  {
    int num = arduino.analogRead(analog);
    float out = map(num,flexMin,flexMax,0,100);
        //println("VALUE: "+ out);
        //print("MAX: "+ flexMax + " ");
        //print("MIN: "+ flexMin);
    return out;

  }
    float getCalibratedValue()
  {
    this.calibrate();
    int num = arduino.analogRead(analog);
    float out = map(num,flexMin,flexMax,0,100);
        //println("VALUE: "+ out);
        //print("MAX: "+ flexMax + " ");
        //print("MIN: "+ flexMin);
    return out;

  }
  
  void calibrate()
{
  int num = arduino.analogRead(analog);
  if( num > flexMax){
    flexMax = num;

  }
  else if(num < flexMin){
    flexMin = num;

  }
  calibrated = true;
}
  void recalibrate()
{
   flexMin = 1000;
   flexMax = 0;
  int num = arduino.analogRead(analog);
  if( num > flexMax){
    flexMax = num;

  }
  else if(num < flexMin){
    flexMin = num;

  }
}

  
}
