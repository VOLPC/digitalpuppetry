/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.Color;
import java.util.ArrayList;

/**
 *
 * @author OCT
 */
public class cGUI extends javax.swing.JFrame {
    private cGlove mGlove;
    private cMapper mapping;
    private ArrayList<javax.swing.JLabel> mLabels = new ArrayList();
    private ArrayList<javax.swing.JComboBox> mDropdowns = new ArrayList();
    private boolean mInitialCalibration;
    /**
     * Creates new form gui
     */
    
    public cGUI() {
        initComponents();
        mapping = new cMapper();
        mGlove = new cGlove();
        messageArea.setOpaque(true);
        mInitialCalibration = false;
        this.setupLabels();
        this.setupDropdowns();
        this.setLabels();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        header = new javax.swing.JLabel();
        thumbID = new javax.swing.JLabel();
        indexID = new javax.swing.JLabel();
        middleID = new javax.swing.JLabel();
        ringID = new javax.swing.JLabel();
        pinkyID = new javax.swing.JLabel();
        thumbDropdown = new javax.swing.JComboBox();
        indexDropdown = new javax.swing.JComboBox();
        middleDropdown = new javax.swing.JComboBox();
        ringDropdown = new javax.swing.JComboBox();
        pinkyDropdown = new javax.swing.JComboBox();
        submitButton = new javax.swing.JButton();
        resetButton = new javax.swing.JButton();
        messageArea = new javax.swing.JLabel();
        thumbMessageLabel = new javax.swing.JLabel();
        indexMessageLabel = new javax.swing.JLabel();
        middleMessageLabel = new javax.swing.JLabel();
        ringMessageLabel = new javax.swing.JLabel();
        pinkyMessageLabel = new javax.swing.JLabel();
        recalibrateButton = new javax.swing.JButton();
        launchGUIPuppetButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(0, 0, 0));

        jPanel1.setBackground(new java.awt.Color(235, 55, 28));

        header.setBackground(new java.awt.Color(235, 55, 28));
        header.setFont(new java.awt.Font("Arial", 2, 36)); // NOI18N
        header.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        header.setText("Puppetry Control Panel");
        header.setOpaque(true);

        thumbID.setForeground(new java.awt.Color(255, 255, 255));
        thumbID.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        thumbID.setText("Thumb");
        thumbID.setMaximumSize(new java.awt.Dimension(40, 14));
        thumbID.setMinimumSize(new java.awt.Dimension(40, 14));
        thumbID.setName(""); // NOI18N

        indexID.setForeground(new java.awt.Color(255, 255, 255));
        indexID.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        indexID.setText("Index");

        middleID.setForeground(new java.awt.Color(255, 255, 255));
        middleID.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        middleID.setText("Middle");

        ringID.setForeground(new java.awt.Color(255, 255, 255));
        ringID.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        ringID.setText("Ring");

        pinkyID.setForeground(new java.awt.Color(255, 255, 255));
        pinkyID.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        pinkyID.setText("Pinky");

        thumbDropdown.setForeground(new java.awt.Color(235, 55, 28));
        thumbDropdown.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Lower Jaw", "Upper Jaw", "Nose", "Eyes", "Ears" }));
        thumbDropdown.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                thumbDropdownActionPerformed(evt);
            }
        });

        indexDropdown.setForeground(new java.awt.Color(235, 55, 28));
        indexDropdown.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Lower Jaw", "Upper Jaw", "Nose", "Eyes", "Ears" }));
        indexDropdown.setSelectedIndex(1);
        indexDropdown.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                indexDropdownActionPerformed(evt);
            }
        });

        middleDropdown.setForeground(new java.awt.Color(235, 55, 28));
        middleDropdown.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Lower Jaw", "Upper Jaw", "Nose", "Eyes", "Ears" }));
        middleDropdown.setSelectedIndex(2);
        middleDropdown.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                middleDropdownActionPerformed(evt);
            }
        });

        ringDropdown.setForeground(new java.awt.Color(235, 55, 28));
        ringDropdown.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Lower Jaw", "Upper Jaw", "Nose", "Eyes", "Ears" }));
        ringDropdown.setSelectedIndex(3);
        ringDropdown.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ringDropdownActionPerformed(evt);
            }
        });

        pinkyDropdown.setForeground(new java.awt.Color(235, 55, 28));
        pinkyDropdown.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Lower Jaw", "Upper Jaw", "Nose", "Eyes", "Ears" }));
        pinkyDropdown.setSelectedIndex(4);
        pinkyDropdown.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pinkyDropdownActionPerformed(evt);
            }
        });

        submitButton.setBackground(new java.awt.Color(255, 255, 255));
        submitButton.setForeground(new java.awt.Color(235, 55, 28));
        submitButton.setText("Submit");
        submitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitButtonActionPerformed(evt);
            }
        });

        resetButton.setBackground(new java.awt.Color(255, 255, 255));
        resetButton.setForeground(new java.awt.Color(235, 55, 28));
        resetButton.setText("Reset");
        resetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetButtonActionPerformed(evt);
            }
        });

        messageArea.setBackground(new java.awt.Color(0, 255, 0));
        messageArea.setForeground(new java.awt.Color(255, 255, 255));
        messageArea.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        messageArea.setText("Assignments are OK");

        thumbMessageLabel.setForeground(new java.awt.Color(255, 255, 255));
        thumbMessageLabel.setText("Select a Thumb assignment");

        indexMessageLabel.setForeground(new java.awt.Color(255, 255, 255));
        indexMessageLabel.setText("Select an Index Finger assignment");

        middleMessageLabel.setForeground(new java.awt.Color(255, 255, 255));
        middleMessageLabel.setText("Select a Middle Finger assignment");

        ringMessageLabel.setForeground(new java.awt.Color(255, 255, 255));
        ringMessageLabel.setText("Select a Ring Finger assignment");

        pinkyMessageLabel.setForeground(new java.awt.Color(255, 255, 255));
        pinkyMessageLabel.setText("Select a Pinky Finger assignment");

        recalibrateButton.setBackground(new java.awt.Color(255, 255, 255));
        recalibrateButton.setForeground(new java.awt.Color(235, 55, 28));
        recalibrateButton.setText("Recalibrate");
        recalibrateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                recalibrateButtonActionPerformed(evt);
            }
        });

        launchGUIPuppetButton.setBackground(new java.awt.Color(255, 255, 255));
        launchGUIPuppetButton.setForeground(new java.awt.Color(235, 55, 28));
        launchGUIPuppetButton.setText("Launch GUI Puppet");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(indexID, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pinkyID, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(thumbID, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(middleID, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ringID, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(ringDropdown, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(ringMessageLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(middleDropdown, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(middleMessageLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(indexDropdown, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(indexMessageLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                            .addComponent(thumbDropdown, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(thumbMessageLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(pinkyDropdown, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(pinkyMessageLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(header, javax.swing.GroupLayout.PREFERRED_SIZE, 554, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(messageArea, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(launchGUIPuppetButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(resetButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(submitButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(recalibrateButton)))))
                .addGap(0, 1, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(header, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(thumbID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(thumbDropdown, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(thumbMessageLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(indexID)
                    .addComponent(indexDropdown, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(indexMessageLabel))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(middleDropdown, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(middleMessageLabel)
                    .addComponent(middleID))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ringDropdown, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ringID)
                    .addComponent(ringMessageLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pinkyID)
                    .addComponent(pinkyDropdown, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pinkyMessageLabel))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(submitButton)
                            .addComponent(resetButton)
                            .addComponent(recalibrateButton))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(launchGUIPuppetButton))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(messageArea, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(27, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>                                                                    

    private void thumbDropdownActionPerformed(java.awt.event.ActionEvent evt) {                                              
        // TODO add your handling code here:
        System.out.println(thumbDropdown.getSelectedIndex());
        mapping.setThumb(thumbDropdown.getSelectedIndex());
        this.setLabels();
                
    }                                             

    private void indexDropdownActionPerformed(java.awt.event.ActionEvent evt) {                                              
        // TODO add your handling code here:
        mapping.setIndex(indexDropdown.getSelectedIndex());
        this.setLabels();
    }                                             

    private void middleDropdownActionPerformed(java.awt.event.ActionEvent evt) {                                               
        // TODO add your handling code here:
        mapping.setMiddle(middleDropdown.getSelectedIndex());
        this.setLabels();
    }                                              

    private void ringDropdownActionPerformed(java.awt.event.ActionEvent evt) {                                             
        // TODO add your handling code here:
        mapping.setRing(ringDropdown.getSelectedIndex());
        this.setLabels();
    }                                            

    private void pinkyDropdownActionPerformed(java.awt.event.ActionEvent evt) {                                              
        // TODO add your handling code here:
        mapping.setPinky(pinkyDropdown.getSelectedIndex());
        this.setLabels();
        
    }                                             

    private void resetButtonActionPerformed(java.awt.event.ActionEvent evt) {                                            
        // TODO add your handling code here:
        this.resetDropdowns();
    }                                           

    private void submitButtonActionPerformed(java.awt.event.ActionEvent evt) {                                             
        // TODO add your handling code here:
        if(setGlove()){
            java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new cCalibrationGUI(mGlove).setVisible(true);
            }
        });
        mInitialCalibration = this.initialCalibration();
      }else{
        messageArea.setText("Initialization error");
      }
        
    }
    private void recalibrateButtonActionPerformed(java.awt.event.ActionEvent evt) {                                                  
        // TODO add your handling code here:
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new cCalibrationGUI(mGlove).setVisible(true);
            }
        });
        mInitialCalibration = this.initialCalibration();
    }    
    
    private boolean checkDuplicates()
    {
        boolean mOK = false;
        mOK = mapping.checkDuplicates();
        submitButton.setEnabled(mOK);
        if(!mOK)
        {
            messageArea.setBackground(Color.red);
            messageArea.setForeground(Color.WHITE);
            messageArea.setText("Assignments are NOT ok!");
            super.setVisible(true);
        }
        else
        {
            messageArea.setBackground(Color.green);
            messageArea.setForeground(Color.BLACK);
            messageArea.setText("Assignments are OK!");
            super.setVisible(true);
        }
        return mOK;
    }
    private void setupLabels()
    {
        mLabels.add(thumbMessageLabel);
        mLabels.add(indexMessageLabel);
        mLabels.add(middleMessageLabel);
        mLabels.add(ringMessageLabel);
        mLabels.add(pinkyMessageLabel);
    }
    private void setupDropdowns()
    {
        mDropdowns.add(thumbDropdown);
        mDropdowns.add(indexDropdown);
        mDropdowns.add(middleDropdown);
        mDropdowns.add(ringDropdown);
        mDropdowns.add(pinkyDropdown);
    }
    private void resetDropdowns()
    {
        for(int i=0;i<mLabels.size();i++)
        {
            mDropdowns.get(i).setSelectedIndex(i);
        }
        this.setLabels();
       
    }
    private void setLabels()
    {
        this.checkDuplicates();
        for(int i=0;i<mLabels.size();i++)
        {
            mLabels.get(i).setText("Assignment set to: "+ mDropdowns.get(i).getSelectedItem()); 
        } 
    }
    private boolean initialCalibration()
    {
        return mapping.initialCalibration();
    }
    private boolean setGlove()
    {
     return mapping.initialize();
    }
    
    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify                     
    private javax.swing.JLabel header;
    private javax.swing.JComboBox indexDropdown;
    private javax.swing.JLabel indexID;
    private javax.swing.JLabel indexMessageLabel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton launchGUIPuppetButton;
    private javax.swing.JLabel messageArea;
    private javax.swing.JComboBox middleDropdown;
    private javax.swing.JLabel middleID;
    private javax.swing.JLabel middleMessageLabel;
    private javax.swing.JComboBox pinkyDropdown;
    private javax.swing.JLabel pinkyID;
    private javax.swing.JLabel pinkyMessageLabel;
    private javax.swing.JButton recalibrateButton;
    private javax.swing.JButton resetButton;
    private javax.swing.JComboBox ringDropdown;
    private javax.swing.JLabel ringID;
    private javax.swing.JLabel ringMessageLabel;
    private javax.swing.JButton submitButton;
    private javax.swing.JComboBox thumbDropdown;
    private javax.swing.JLabel thumbID;
    private javax.swing.JLabel thumbMessageLabel;
    // End of variables declaration                        
}

