class cMapper {
  
  private int[] controls;
  boolean mappingOK = false;
  boolean initialized= false;

  /*  Thumb = 0
      Index = 1
      Middle = 2
      Ring = 3
      pinky = 4
  */
  /*  upper jaw = 0
      lower jaw = 1
      Blink = 2
      Nose = 3
      Eyes = 4
  */
  
  cMapper(){
    controls = new int[5];
    for(int i=0;i<5;i++){
      controls[i]=i;
    }

  }
  public boolean initialize(){
    if(checkDuplicates()){
      initialized = true;
      return initialized;
    }else{
      initialized = false;
      return initialized;
    }
    
  }
  public void mapFunctions(int sensor, int function)
  {
    controls[sensor] = function;
  }
  public boolean checkCalibration()
  {
    boolean ok = true;
    
    return ok;
  }
  public boolean initialCalibration()
  {
      boolean calibrationOK =true;
      return calibrationOK;
  }
  
  public void setThumb(int num)
  {
    controls[0]=num;
  }
  public void setIndex(int num)
  {
    controls[1]=num;
  }
  public void setMiddle(int num)
  {
    controls[2]=num;
  }  
  public void setRing(int num)
  {
    controls[3]=num;
  }
  public void setPinky(int num)
  {
    controls[4]=num;
  }

  public int getThumb()
  {
    return controls[0];
  }

  public boolean checkDuplicates(){
    mappingOK=true;
    for (int j=0;j<controls.length;j++){
      for (int k=j+1;k<controls.length;k++){
        if (controls[k] == controls[j]){
          mappingOK=false;
        }
        
      }
    }
    return mappingOK;    
  }
public void printList()
{
  for(int i=0;i<5;i++)
  {
    print(controls[i] +" ");
  }
}
  
}
